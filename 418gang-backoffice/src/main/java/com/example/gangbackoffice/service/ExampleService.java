package com.example.gangbackoffice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gangbackoffice.repository.ExampleRepository;

@Service
public class ExampleService {
    
    @Autowired
    private ExampleRepository exampleRepository;

    public ExampleRepository getRepo() {
        return exampleRepository;
    }

    // Could add more methods here to do more complex things with the repository
    // like cross-referencing with other repositories, etc.
}
