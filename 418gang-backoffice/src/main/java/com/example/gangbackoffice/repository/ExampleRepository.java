package com.example.gangbackoffice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.gangbackoffice.entity.ExampleEntity;

// This will be AUTO IMPLEMENTED by Spring
// It holds a lot of useful methods but should
// be used through the service class

@Repository
public interface ExampleRepository
        extends JpaRepository<ExampleEntity, Long> {
 
}
