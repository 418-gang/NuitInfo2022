package com.example.gangbackoffice.controller;

import org.apache.tomcat.util.json.JSONParser;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class HelloController {
    @CrossOrigin
    @GetMapping("/hello")
    public String index() {
        // ObjectMapper mapper = new ObjectMapper();
        // try {
        //     return mapper.writeValueAsString("Hello");
        // } catch (JsonProcessingException e) {
        //     return "ERROR AT JSON PARSING";
        // }
        return "\"Hello\"";
    }

}
