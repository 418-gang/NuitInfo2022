# Mettre en place docker

1. Installer docker et docker-compose

2. Lancer le service docker:

    ```bash
    sudo systemctl start docker.service
    ```

3. Construire l'environnement docker:

    ```bash
    make up
    ```

4. C'est tout! (Obtenir de l'aide avec `make help`)
