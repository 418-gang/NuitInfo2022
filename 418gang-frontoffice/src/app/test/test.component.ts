import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  public data : String = "Waiting for server...";
  constructor(public http : HttpClient) { }

  ngOnInit(): void {
    this.http.get<String>('http://localhost:8080/hello').subscribe(data => {
        console.log(data);
        this.data = data;
    }) 
  }

}
